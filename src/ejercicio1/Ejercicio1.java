/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;
import Automotores.Carro;
import java.util.*;


/**
 * int: Declarar numeros enteros
 * double: Declarar numeros decimales (permite enteros)
 * String: Tipo texto
 * 
 * 
 */

/**
 *
 * @author s109e20
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Llamado al procedimiento de imprimir nombre
        imprimirnombre();
        
        //Ingreso de informacion por teclado
        Scanner miScanner = new Scanner(System.in);
                  
        //Captura el numero uno
        System.out.print("Por favor ingrese el numero uno para realizar las operaciones:"); 
        double numerouno = miScanner.nextDouble(); 
        System.out.println("El datos ingresado es:" + numerouno);
             
        //Captura el numero dos
        System.out.print("Por favor ingrese el numero dos para realizar las operaciones:"); 
        double numerodos = miScanner.nextDouble(); 
        System.out.println("El datos ingresado es:" + numerodos);        
        
        // Lamado funcion sumar numeros
        double resultadosuma = sumarnumeros(numerouno, numerodos);
        imprimirresultado(resultadosuma, "SUMA");
       
        // Lamado funcion restar numeros
        double resultadoresta = restarnumeros(numerouno, numerodos);
        imprimirresultado(resultadoresta, "RESTA");
        
        // Lamado funcion dividir numeros
        double resultadodividir = dividirnumeros(numerouno, numerodos);
        imprimirresultado(resultadodividir, "DIVIDIR");
        
        // Lamado funcion multiplicar numeros
        double resultadomultiplica = multiplicanumeros(numerouno, numerodos);
        imprimirresultado(resultadomultiplica, "MULTIPLICAR");
        
    
    Carro miprimercarro = new Carro("AZUL", 4, 2010, "KIA");
    Carro misegundocarro = new Carro("NEGRO", 3, 2012, "RENAULT");
   
    double valorprimercarro = miprimercarro.valorcarro();
    double valorsegundocarro = misegundocarro.valorcarro();
    
    System.out.println("El valor del primer carro es:" + valorprimercarro);
    System.out.println("El valor del segundo carro es:" + valorsegundocarro);
    miprimercarro.imprimir();
    misegundocarro.imprimir();
    
    
   
    }
    /**
     * Procedimiento para imprimir un nombre
     */
        private static void imprimirnombre(){
            //Comando para imprimir en consola
        System.out.println("Mi nombre es LUIS GALLEGO");
    }
    
    /**
     * Función para sumar dos numeros
     * @parameter sumarnumeros
     * @return resultadosuma
     * 
     */    
     
    /**
     * Función para sumar numeros
     */    
         private static double sumarnumeros(double numerouno,double numerodos){
            double resultadosuma = numerouno + numerodos;
            return resultadosuma;
    }
     
    /**
     * Función para restar numeros
     */     
         private static double restarnumeros(double numerouno,double numerodos){
         double resultadoresta = numerouno - numerodos;
         return resultadoresta;
    }
     
    /**
     * Función para dividir numeros
     */     
         private static double dividirnumeros(double numerouno,double numerodos){
         double resultadodividir = numerouno / numerodos;
         return resultadodividir;
    }    
     
    /**
     * Procedimiento para multiplicar numeros
     */     
         private static double multiplicanumeros(double numerouno,double numerodos){
         double resultadomultiplica = numerouno * numerodos;
         return resultadomultiplica;
    }
         
     /**
      * Metodo para imprimir resultados de cualquier operación
      */    
         private static void imprimirresultado(double resultado, String tipooperacion){
             System.out.println("La operación realizada es:" + tipooperacion);
             System.out.println("Resultado es:" + resultado);
     }
}